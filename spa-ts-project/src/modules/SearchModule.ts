import {
  VuexModule,
  Module,
  MutationAction,
  Mutation,
  Action
} from "vuex-module-decorators";
import { Album } from "../models/Album";
import { musicSearch } from "../services";

@Module({
  namespaced: true
})
export default class SearchModule extends VuexModule {
  // State
  items: Album[] = [];
  searchQuery: string = "batman";
  isLoading: boolean = false;
  error: Error | null = null;

  @Mutation
  setQuery(query: string) {
    this.searchQuery = query;
  }

  @Mutation
  setError(error: Error | null) {
    this.error = error;
  }

  @Mutation
  setLoading(loading: boolean) {
    this.isLoading = loading;
  }

  @Mutation
  loadResults(results: Album[]) {
    this.items = results;
  }

  /*  @Action
  search(query: string) {
    const { commit } = this.context;
    //
    commit("setQuery", query);
    commit("setLoading", true);
    commit("setError", null);
    commit("loadResults", []);

    musicSearch
      .search(query)
      .then(albums => commit("loadResults", albums))
      .catch(error => commit("setError", error))
      .finally(() => commit("setLoading", false));
  } */

  /* @Action
  resetQuery(query: string) {
    const { commit } = this.context;

    commit("setQuery", query);
    commit("setLoading", true);
    commit("setError", null);
    commit("loadResults", []);
  } */

  @MutationAction({
    mutate: ["searchQuery", "isLoading", "error", "items"]
  })
  async resetQuery(query: string) {
    return {
      searchQuery: query,
      isLoading: false,
      error: null,
      items: []
    };
  }

  @Action
  async search(query: string) {
    const { commit, dispatch } = this.context;

    dispatch("resetQuery", query);

    try {
      const albums = await musicSearch.search(query);
      commit("loadResults", albums);
    } catch (error) {
      commit("setError", error);
    } finally {
      commit("setLoading", false);
    }
  }

  get loading() {
    return this.isLoading;
  }
  get results() {
    return this.items;
  }

  get errorMessage() {
    return this.error && this.error.message;
  }

  get query() {
    return this.searchQuery;
  }
}
