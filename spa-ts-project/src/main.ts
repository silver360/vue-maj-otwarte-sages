import Vue from "vue";
import App from "./App.vue";
import router from "./router";
import store from "./store";
import "./registerServiceWorker";
import { authService } from "./services";

authService.getToken();

Vue.config.productionTip = false;

// this.$http.get(...)
// https://github.com/pagekit/vue-resource/

(window as any).store = store;

new Vue({
  data: {
    auth: authService
  },
  router,
  store,
  // template:'<div>ala ma kota {{1+2}}</div>'
  render: h => h(App)
}).$mount("#app");
