import Vue from "vue";
import App from "./App.vue";

Vue.config.productionTip = false;

new Vue({
  render: h => h(App),
  // render: h => h('div',{id:'placki'},App)
  // template:'<div>Lubie {{thing}}</div>',
  // render: h =>
  //   h(
  //     "div",
  //     {}, //
  //     h(
  //       "p",
  //       { class: "awesome" }, //
  //       "Lubie " + this.thing
  //     )
  //   )
}).$mount("#app");
