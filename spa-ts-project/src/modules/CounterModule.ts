import { Module } from "vuex";
interface CounterState {
  counter: number;
  stoper_id: number;
}
export  const CounterModule: Module<CounterState, {}> = {
  state: {
    counter: 0,
    stoper_id: 0
  },
  mutations: {
    increment(state, payload = 1) {
      state.counter += payload;
    },
    decrement(state, payload = 1) {
      state.counter -= payload;
    },
    stoper_id(state, stoper_id) {
      state.stoper_id = stoper_id;
    }
  },
  actions: {
    stoper({ commit, dispatch, state, getters }) {
      const id = setInterval(() => {
        commit("increment");
      }, 2000);
      commit("stoper_id", id);
    }
  },
  getters: {
    counterx2(state) {
      return state.counter * 2;
    }
  }
};
