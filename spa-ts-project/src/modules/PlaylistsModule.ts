import {
  Module,
  VuexModule,
  Mutation,
  Action,
  MutationAction
} from "vuex-module-decorators";
import { Playlist } from "../models/Playlist";
// https://github.com/championswimmer/vuex-module-decorators

enum Mutations {}
enum Actions {}

@Module({
  namespaced: true
})
export default class PlaylistsModule extends VuexModule {
  // State:
  list: Playlist[] = [
    {
      id: 123,
      name: "Vue Hits!",
      favourite: false,
      color: "#ff00ff"
    },
    {
      id: 234,
      name: "Vue Top20!",
      favourite: true,
      color: "#ffff00"
    },
    {
      id: 345,
      name: "Best of Vue!",
      favourite: false,
      color: "#00ffff"
    }
  ];
  selectedId: Playlist["id"] | null = null;
  filterQuery: string = "";

  @Mutation add(payload: Playlist) {
    this.list.push(payload);
  }

  @Mutation update(payload: Playlist) {
    const index = this.list.findIndex(p => p.id == payload.id);
    if (index !== -1) {
      this.list.splice(index, 1, payload);
    }
  }

  @Mutation load(payload: Playlist[]) {
    this.list = payload;
  }

  @Mutation selectId(id: Playlist["id"]) {
    this.selectedId = id;
  }

  @Mutation setQuery(query: string) {
    this.filterQuery = query;
  }

  // Actions:
  @Action({ commit: "selectId" })
  select(selected: Playlist | null) {
    return selected && selected.id;
  }

  @Action({}) search(query: string) {
    if (query !== "") {
      this.context.commit("setQuery", query);
    }
  }

  @Action({ commit: "update" }) save(draft: Playlist) {
    return draft;
  }

  // Getters:
  get playlists() {
    return this.list;
  }

  get selected() {
    return this.list.find(p => p.id == this.selectedId);
  }

  get query() {
    return this.filterQuery;
  }

  get results() {
    return this.playlists.filter(p =>
      p.name.toLowerCase().includes(this.query.toLowerCase())
    );
  }
}
