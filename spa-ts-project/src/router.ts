import Vue from "vue";
import Router, { Route } from "vue-router";
import Home from "./views/Home.vue";
import MusicSearch from "./search/MusicSearch.vue";
import Playlists from "./playlists/Playlists.vue";
import PlaylistDetails from "./playlists/components/PlaylistDetails.vue";
import PlaylistForm from "./playlists/components/PlaylistForm.vue";

Vue.use(Router);

export default new Router({
  // mode:'hash',
  mode: "history",
  linkActiveClass: "active",
  routes: [
    {
      path: "/",
      name: "home",
      // component: Home,
      redirect: "/playlists"
    },
    {
      path: "/music",
      name: "search",
      component: MusicSearch
    },
    {
      path: "/playlists",
      name: "playlists",
      component: Playlists,
      beforeEnter(to: Route, from: Route, next) {
        next();
      }
    },
    /* [vue-router] Named Route 'playlists_details' has a default child route. When navigating to this named route (:to="{name: 'playlists_details'"), the default child route will not be rendered. Remove the name from this route and use the name of the default child route for named links instead. */
    {
      path: "/playlists/:id",
      component: Playlists,
      children: [
        {
          path: "",
          name: "playlists_details",
          component: PlaylistDetails
        },
        {
          path: "edit",
          component: PlaylistForm
        }
      ]
    },
    {
      path: "/about",
      name: "about",
      // route level code-splitting
      // this generates a separate chunk (about.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () =>
        import(/* webpackChunkName: "about" */ "./views/About.vue")
    },
    {
      path: "*",
      redirect: "/"
    }
  ]
});
