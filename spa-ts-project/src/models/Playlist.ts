export interface Entity {
  id: number;
  name: string;
}

export interface Playlist extends Entity {
  favourite: boolean;
  /**
   * Color in Hex
   */
  color: string;
  tracks?: Track[];

  // tracks: Array<Track>
  // tracks: Track[] | undefined;
}

export interface Track extends Entity {}
